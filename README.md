# weather-app-python
 Weather App with API in python


----

## requirements 
   
    $ pip install -r requirements.txt

----

## Setup
To get your API please visit [here](https://openweathermap.org/api/)

* setup your API-KEY:

### auth.py
    apikey='YOUR-API-KEY' 
    
----
## Deployment
* check out live deployment [here](https://weather-app-python.herokuapp.com) 


----
## Thanks
* Thanks for checking it out.  :) 
